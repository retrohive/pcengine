# PC-Engine

based on https://archive.org/details/no-intro_romsets (NEC - PC Engine - TurboGrafx 16 (20230116-063321))

ROM not added :
```sh
❯ tree -hs --dirsfirst                                                                                                                                                                                                                       mer. 01 févr. 2023 21:42:16
[4.0K]  .
├── [512K]  Air Zonk (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Atlantean (World) (Aftermarket) (Homebrew).pce
├── [256K]  [BIOS] CD-ROM System (Japan) (v1.0).pce
├── [256K]  [BIOS] CD-ROM System (Japan) (v2.0).pce
├── [256K]  [BIOS] CD-ROM System (Japan) (v2.1).pce
├── [ 32K]  [BIOS] Games Express CD Card (Japan) (Blue Version).pce
├── [ 16K]  [BIOS] Games Express CD Card (Japan) (Green Version).pce
├── [256K]  [BIOS] Super CD-ROM System (Japan) (v3.0).pce
├── [256K]  [BIOS] TurboGrafx CD Super System Card (USA) (v3.0).pce
├── [256K]  [BIOS] TurboGrafx CD System Card (USA) (v2.0).pce
├── [1.0M]  Bonk 3 - Bonk's Big Adventure (USA, Europe) (Wii U Virtual Console).pce
├── [384K]  Bonk's Adventure (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Bonk's Revenge (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Bonk's Revenge (USA, Europe) (Wii Virtual Console).pce
├── [384K]  Devil Crash - Naxat Pinball (Japan) (Wii U Virtual Console).pce
├── [384K]  Devil's Crush (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Dinoforce (World) (Aftermarket) (Homebrew).pce
├── [256K]  Double Dungeons (Japan) (Wii U Virtual Console).pce
├── [256K]  Double Dungeons (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Final Soldier (World) (Wii U Virtual Console).pce
├── [384K]  Gai Flame (Japan) (Wii U Virtual Console).pce
├── [256K]  Gradius (World) (Wii U Virtual Console).pce
├── [256K]  HuZERO (World) (Aftermarket) (Homebrew).pce
├── [256K]  Jaseiken Necromancer (Japan) (Wii Virtual Console).pce
├── [256K]  Jaseiken Necromancer (World) (Wii U Virtual Console).pce
├── [256K]  Kung Fu, The (Japan) (Wii Virtual Console).pce
├── [768K]  Neutopia II (Japan) (Wii U Virtual Console).pce
├── [768K]  Neutopia II (USA, Europe) (Wii U Virtual Console).pce
├── [384K]  Neutopia (Japan) (Wii U Virtual Console).pce
├── [384K]  Neutopia (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  New Adventure Island (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Ninja Spirit (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  PC Denjin - Punkic Cyborgs (Japan) (Wii U Virtual Console).pce
├── [512K]  PC Genjin 2 - Pithecanthropus Computerurus (Japan) (Wii U Virtual Console).pce
├── [512K]  PC Genjin 2 - Pithecanthropus Computerurus (Japan) (Wii Virtual Console).pce
├── [1.0M]  PC Genjin 3 - Pithecanthropus Computerurus (Japan) (Wii U Virtual Console).pce
├── [384K]  PC Genjin - Pithecanthropus Computerurus (Japan) (Wii U Virtual Console).pce
├── [512K]  Power Sports (Japan) (Wii U Virtual Console).pce
├── [512K]  Saigo no Nindou - Ninja Spirit (Japan) (Wii U Virtual Console).pce
├── [256K]  Salamander (World) (Wii U Virtual Console).pce
├── [512K]  Soldier Blade (Japan) (Wii U Virtual Console).pce
├── [512K]  Soldier Blade (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Super Star Soldier (Japan) (Wii U Virtual Console).pce
├── [512K]  Super Star Soldier (USA, Europe) (Wii U Virtual Console).pce
├── [512K]  Takahashi Meijin no Shin Bouken-jima (Japan) (Wii U Virtual Console).pce
├── [512K]  Tongueman's Logic (World) (Aftermarket) (Homebrew).pce
├── [512K]  Tongueman's Logic (World) (Demo) (Aftermarket) (Homebrew).pce
├── [256K]  UWOL - Quest for Money (World) (v1.1) (Aftermarket) (Homebrew).pce
├── [256K]  Victory Run - Eikou no 13,000km (Japan) (Wii U Virtual Console).pce
├── [256K]  Victory Run (USA, Europe) (Wii U Virtual Console).pce
├── [384K]  Wallaby!! - Usagi no Kuni no Kangaroo Race (Japan) (Wii U Virtual Console).pce
├── [256K]  World Class Baseball (USA, Europe) (Wii Virtual Console).pce
└── [512K]  World Sports Competition (USA, Europe) (Wii U Virtual Console).pce

1 directory, 53 files
```
